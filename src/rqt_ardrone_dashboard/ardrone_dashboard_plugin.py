#!/usr/bin/env python

import os
import sys
from threading import Lock

# import QT
from PyQt4 import QtCore

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtGui import QLabel, QTreeWidget, QTreeWidgetItem, QVBoxLayout, QCheckBox, QWidget, QToolBar, QLineEdit, QPushButton
from python_qt_binding.QtCore import Qt, QTimer

# import ROS
import rospy
import roslib

# import ROS msgs
from ardrone_autonomy.msg import Navdata
# import ROS srvs
from std_srvs.srv import Empty

# import
from ardrone_status import ARdroneStatus

#constants
REDRAW_FREQ = 30 #[hz]

class ARDroneDashboard(Plugin):
   StatusMessages = {
      ARdroneStatus.Emergency : 'Emergency',
      ARdroneStatus.Inited    : 'Initialized',
      ARdroneStatus.Landed    : 'Landed',
      ARdroneStatus.Flying    : 'Flying',
      ARdroneStatus.Hovering  : 'Hovering',
      ARdroneStatus.Test      : 'Test',
      ARdroneStatus.TakingOff : 'Taking Off',
      ARdroneStatus.GotoHover : 'Going to Hover Mode',
      ARdroneStatus.Landing   : 'Landing',
      ARdroneStatus.Looping   : 'Looping'
   }
   UnknownStatus = 'Unknown Status'

   def __init__(self,context):
      # init RQT gui
      super(ARDroneDashboard, self).__init__(context)
      self.setObjectName('ARDroneDashboard')
      from argparse import ArgumentParser
      parser = ArgumentParser()
      parser.add_argument("-q", "--quiet", action="store_true",
                        dest="quiet",
                        help="Put plugin in silent mode")
      args, unknowns = parser.parse_known_args(context.argv())
      self._widget = QWidget()
      ui_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'dashboard.ui')
      loadUi(ui_file, self._widget)
      self._widget.setObjectName('ARdroneUi')
      if context.serial_number() > 1:
         self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
      context.add_widget(self._widget)

      '''
      init GUI
      '''
      #toggle cam btn callback
      self._widget.btn_toggle_cam.clicked.connect(self.toggleCamCallback)
  
      # init service proxy
      self.srvToggleCam = rospy.ServiceProxy('/ardrone/togglecam', Empty)
  
      # Subscribe to ardrone navdata
      self.subNavdata = rospy.Subscriber('/ardrone/navdata', Navdata, self.ReceiveNavdata)

      # A timer to redraw the GUI
      self.redrawTimer = QtCore.QTimer(self)
      self.redrawTimer.timeout.connect(self.RedrawCallback)
      self.redrawTimer.start(1000.0 * 1.0/REDRAW_FREQ)

      '''
      init variables
      '''
      self.navdata = None
      self.navdataLock = Lock()    

   def RedrawCallback(self):
      self.navdataLock.acquire()
      try:
         if self.navdata != None:
            status_msg = self.StatusMessages[self.navdata.state] if self.navdata.state in self.StatusMessages else self.UnknownStatus
      
            self._widget.state.setText(status_msg)

            self._widget.battery.setText(str(self.navdata.batteryPercent) + '%')

            #vel
            self._widget.vx.setText(str(self.navdata.vx/1000.0)) #scale from [mm/s] to [m/s]
            self._widget.vy.setText(str(self.navdata.vy/1000.0))
            self._widget.vz.setText(str(self.navdata.vz/1000.0))
            #acc
            self._widget.ax.setText(str(self.navdata.ax))
            self._widget.ay.setText(str(self.navdata.ay))
            self._widget.az.setText(str(self.navdata.az))
   
      finally:
         self.navdataLock.release()   

   def ReceiveNavdata(self,msg):
      self.navdataLock.acquire()
      try:
         self.navdata = msg
      finally:
         self.navdataLock.release()

   def toggleCamCallback(self,event):
      try:
         self.srvToggleCam()
      except rospy.ServiceException, e:
         pass

   def shutdown_plugin(self):
      pass

   def save_settings(self, plugin_settings, instance_settings):
      pass

   def restore_settings(self, plugin_settings, instance_settings):
      pass

   #def trigger_configuration(self):
      # Comment in to signal that the plugin has a way to configure it
      # Usually used to open a configuration dialog
   
